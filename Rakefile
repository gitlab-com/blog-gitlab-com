require 'yaml'
require 'stringex'
require 'time'
require 'middleman'

task :setup_git do
  puts "\n=> Setting up dummy user/email in Git"
  `git config user.name "John Doe"`
  `git config user.email johndoe@example.com`
end

desc 'Setup repositories for www-gitlab-com in special way exposing only relevant directories'
task :setup_repos do
  puts "\n=> Setting up website"
  `git init tmp/website/`

  Dir.chdir('tmp/website/') do
    `git remote add origin https://gitlab.com/gitlab-com/www-gitlab-com.git`
    # Configure repository for sparse-checkout
    `git config core.sparsecheckout true`
    File.open('.git/info/sparse-checkout', 'w') do |post|
      post.puts '/bin/*'
      post.puts '/data/*'
      post.puts '/lib/*'
      post.puts '/extensions/*'
      post.puts '/helpers/*'
      post.puts '/scripts/*'
      post.puts '/source/layouts/*'
      post.puts '/source/includes/*'
      post.puts '/source/atom.xml'
      post.puts '/source/category.html.haml'
    end
  end
end

desc 'Pulls down the www-gitlab-com git repo fetching and keeping only the most recent commit'
task :pull_repos do
  puts "\n=> Pulling website repo \n"
  # Enter the temporary directory and return after block is completed.
  Dir.chdir('tmp/website/') do
    `git fetch origin master --depth 1`
    # Reset so that if the repo is cached, the latest commit will be used
    `git reset --hard origin/master`
  end
end

desc 'Setup content directories by symlinking to the direcotires and files'
task :setup_content_dirs do
  source_bin = File.join('tmp/website/', 'bin')
  source_data = File.join('tmp/website/', 'data')
  source_lib = File.join('tmp/website/', 'lib')
  source_extensions = File.join('tmp/website/', 'extensions')
  source_helpers = File.join('tmp/website/', 'helpers')
  source_scripts = File.join('tmp/website/', 'scripts')
  source_layouts = File.join('tmp/website/source/', 'layouts')
  source_includes = File.join('tmp/website/source/', 'includes')
  source_atom = File.join('tmp/website/source/', 'atom.xml')
  source_categories = File.join('tmp/website/source/', 'category.html.haml')
  source_root = '.'
  source_target = 'source'

  next if File.symlink?(source_target)

  puts "\n=> Setting up content directory for bin \n"
  `cp -R #{source_bin} #{source_root}`
  puts "\n=> Setting up content directory for data \n"
  `cp -R #{source_data} #{source_root}`
  puts "\n=> Setting up content directory for lib \n"
  `cp -R #{source_lib} #{source_root}`
  puts "\n=> Setting up content directory for extensions \n"
  `cp -R #{source_extensions} #{source_root}`
  puts "\n=> Setting up content directory for helpers \n"
  `cp -R #{source_helpers} #{source_root}`
  puts "\n=> Setting up content directory for scripts \n"
  `cp -R #{source_scripts} #{source_root}`
  puts "\n=> Setting up content directory for layouts \n"
  `cp -R #{source_layouts} #{source_target}`
  puts "\n=> Setting up content directory for includes \n"
  `cp -R #{source_includes} #{source_target}`
  puts "\n=> Setting up content directory for atom.xml \n"
  `cp -R #{source_atom} #{source_target}`
  puts "\n=> Setting up content directory for categories.html.haml \n"
  `cp -R #{source_categories} #{source_target}`
end

desc 'Begin a new post'
task :new_post, :title do |t, args|
  if args.title
    title = args.title
  else
    puts 'Enter a title for your post: '
    title = STDIN.gets.chomp
  end

  filename = "source/posts/#{Time.now.strftime('%Y-%m-%d')}-#{title.to_url}.html.md.erb"
  puts "Creating new post: #{filename}"
  File.open(filename, 'w') do |post|
    post.puts '---'
    post.puts "title: \"#{title.gsub(/&/, '&amp;')}\""
    post.puts 'author: Firstname Lastname # if name includes special characters use double quotes "First Last"'
    post.puts 'author_gitlab: GitLab.com username # ex: johndoe'
    post.puts 'author_twitter: Twitter username or gitlab # ex: johndoe'
    post.puts 'categories: company'
    post.puts 'image_title: "/images/blogimages/post-cover-image.jpg"'
    post.puts 'description: "Short description for the blog post"'
    post.puts 'tags: tag1, tag2, tag3'
    post.puts 'cta_button_text: "Watch the <strong>XXX release webcast</strong> live!" # optional'
    post.puts 'cta_button_link: "https://page.gitlab.com/xxx.html" # optional'
    post.puts 'guest: false # required when the author is not a GitLab Team Member'
    post.puts 'ee_cta: false # required only if you do not want to display the EE-trial banner'
    post.puts 'install_cta: false # required only if you do not want to display the "Install GitLab" banner'
    post.puts "twitter_text: \"Text to tweet\" # optional;  If no text is provided it will use post's title."
    post.puts 'featured: yes # reviewer should set'
    post.puts '---'
  end
end

desc 'Build the site in public/ (for deployment)'
task :build do
  build_cmd = %w[middleman build --bail]
  raise "command failed: #{build_cmd.join(' ')}" unless system(*build_cmd)
end
