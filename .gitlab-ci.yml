# Note that the rspec job below uses a different image that also
# includes chromedriver. If we update the Ruby version for this image,
# we should also update it for the rspec job.
image: registry.gitlab.com/gitlab-org/gitlab-build-images:www-gitlab-com-2.6

variables:
  GIT_DEPTH: "10"
  # Speed up middleman
  NO_CONTRACTS: "true"

.install: &install
  bundle install --jobs 4 --path vendor

# This injects master, merge requests, and tags as default only behavior
# in order to have all jobs run in all cases for these refs, except where
# otherwise intended (for example, to only create review apps for MRs.)
.only-default: &only-default
  only:
    - master
    - merge_requests
    - tags

before_script: [*install]

cache:
  key: "web_ruby-2.6-stretch"
  paths:
    - tmp/cache
    - vendor

stages:
  - build
  - deploy

rubocop:
  <<: *only-default
  stage: build
  script:
    - bundle exec rubocop
  tags:
    - gitlab-org

.build_base: &build_base
  script:
    # We only want the images to be cropped in the second job, as this is the image job
    - *install
    - bundle exec rake setup_git setup_repos pull_repos setup_content_dirs
    - bundle exec rake build
  stage: build
  artifacts:
    expire_in: 7 days
    paths:
      - public/
  tags:
    - gitlab-org

build_branch:
  <<: *build_base
  only:
    - merge_requests
    - tags
  except:
    - master

build_master:
  <<: *build_base
  variables:
    MIDDLEMAN_ENV: 'production'
  only:
    - master

dependency_scanning:
  <<: *only-default
  stage: build
  image: docker:stable
  allow_failure: true
  before_script: []
  cache: {}
  dependencies: []
  tags: []
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json

review:
  stage: deploy
  allow_failure: true
  before_script: []
  cache: {}
  dependencies:
    - build_branch
  variables:
    GIT_STRATEGY: none
  script:
    # Force all images to use absolute URL - https://about.gitlab.com/images
    - >
      find public/ -type f -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#src='/images#src='https://about.gitlab.com/images#g" "{}" +;
    # Force more images to use absolute URL - https://about.gitlab.com/images
    - >
      find public/ -type f -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#src=\"/images#src=\"https://about.gitlab.com/images#g" "{}" +;
    # Force some more images to use absolute URL - https://about.gitlab.com/images
    - >
      find public/ -type f -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#url(/images#url(https://about.gitlab.com/images#g" "{}" +;
    # Force all stylesheets to use absolute URL - https://about.gitlab.com/stylesheets
    - >
      find public/ -type f -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#href=\"/stylesheets#href=\"https://about.gitlab.com/stylesheets#g" "{}" +;
    # Force more stylesheets to use absolute URL - https://about.gitlab.com/stylesheets
    - >
      find public/ -type f -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#href='/stylesheets#href='https://about.gitlab.com/stylesheets#g" "{}" +;
    # Force all javascript to use absolute URL - https://about.gitlab.com/javascripts
    - >
      find public/ -type f -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#src='/javascripts#src='https://about.gitlab.com/javascripts#g" "{}" +;
    #- ./bin/combine-sitemaps
    - rsync --ignore-times --checksum --delete -avz public ~/pages/blog-$CI_COMMIT_REF_SLUG
  # Clean up the current working directory, as all review jobs share the same directory
  after_script:
    - rm -rf ./public/
  environment:
    name: review/blog-$CI_COMMIT_REF_SLUG
    url: https://blog-$CI_COMMIT_REF_SLUG.about.gitlab-review.app
    on_stop: review_stop
  only:
    - merge_requests@gitlab-com/blog-gitlab-com
  except:
    - master@gitlab-com/blog-gitlab-com
  tags:
    - review-apps

review_stop:
  stage: deploy
  before_script: []
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf ~/pages/blog-$CI_COMMIT_REF_SLUG
  when: manual
  environment:
    name: review/blog-$CI_COMMIT_REF_SLUG
    action: stop
  only:
    - merge_requests@gitlab-com/blog-gitlab-com
  except:
    - master@gitlab-com/blog-gitlab-com
  tags:
    - review-apps

# .gcp-deploy-base: &gcp-deploy-base
#   image: google/cloud-sdk:latest
#   stage: deploy
#   cache: {}
#   variables:
#     GIT_STRATEGY: none
#   dependencies:
#     - build_master
#   script:
#     - ./bin/combine-sitemaps
#     - echo "$GCP_SERVICE_ACCOUNT_KEY" > key.json
#     - gcloud auth activate-service-account --key-file key.json
#     - gcloud config set project $GCP_PROJECT
#     - gsutil -m rsync -c -d -r public/ gs://$GCP_BUCKET
#   only:
#     - master@gitlab-com/www-gitlab-com
# 
# deploy_staging:
#   <<: *gcp-deploy-base
#   before_script:
#     - export GCP_PROJECT=$GCP_PROJECT_STAGING
#     - export GCP_BUCKET=$GCP_BUCKET_STAGING
#     - export GCP_SERVICE_ACCOUNT_KEY=$GCP_SERVICE_ACCOUNT_KEY_STAGING
#   environment:
#     name: staging
#     url: https://about.staging.gitlab.com
# 
# deploy:
#   <<: *gcp-deploy-base
#   before_script:
#     - export GCP_PROJECT=$GCP_PROJECT_PRODUCTION
#     - export GCP_BUCKET=$GCP_BUCKET_PRODUCTION
#     - export GCP_SERVICE_ACCOUNT_KEY=$GCP_SERVICE_ACCOUNT_KEY_PRODUCTION
#   environment:
#     name: production
