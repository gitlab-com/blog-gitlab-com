## The GitLab Blog

-- Notice ---

This is the WORK IN PROGRESS for pulling the GitLab Blog into it's own dedicated repository.

-- Production README.md, in progress --

This project generates the GitLab
Blog website and deploys to https://about.gitlab.com/blog. It uses Middleman to compile the following static webpages:
* https://about.gitlab.com/blog
* https://about.gitlab.com/posts/* 

It's built to be a content package repository for blog posts. So if you need to create and publish a blog post - this is the right repository to do that. Anything else associated with the about.gitlab.com website, will need to take place within the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) repository. 

### Creating a Blog Post
To create a blog post, please follow the instructions located in the handbook here:
https://about.gitlab.com/handbook/marketing/blog/

### This is a `git submodule` repository 
The repository is a `git submodule` of the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) repository. `git sumodule` allows you to treat the two projects as separate yet still be able to use one from within the other. 

The following directories are used directly from the `www-gitlab-com` repository: 
* `/bin/.`
* `/data/.`
* `/extensions/.`
* `/helpsers/.`
* `/lib/.`
* `/scripts/.`
* `/source/layouts/.`
* `/source/includes/.`

This means that any updates to those direcotries or the files within, must be completed within the www-gitlab-com repository. 

For a guide on how to start editing the website using git, see the [handbook page on that topic](https://about.gitlab.com/handbook/git-page-update).

### Local Development

Instructions once https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34973 is merged will live within the www-gitlab-com repository since a `git clone` of www-gitlab-com will include the `blog-gitlab-com`submodule.
```
git clone --recurse-submodules git@gitlab.com:gitlab-com/www-gitlab-com.git
cd www-gitlab-com
NO_CONTRACTS=yes bundle exec middleman server
```

Instructions while in development
```sh
git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
cd www-gitlab-com
git checkout 5847-configure-for-separate-blog-repository
cd blog-gitlab-com
git submodule init
git submodule update
cd ../
NO_CONTRACTS=yes bundle exec middleman server
```
### Working with submodules
Always commit your changes to the blog-gitlab-com submdoule first before commiting to the www-gitlab-com submdoule. 

### Commiting a blog post
```
cd blog-gitlab-com`
git status
git add .
git commit -m "message about blog post"
git push origin BRANCH-NAME


### Deployment

TBD